const express = require ('express');
const ruta = express.Router();

const controladorInventario = require('../Controladores/ControladorInventario');

ruta.post('/inventarioActualizacion/:pk_id_estante', controladorInventario.Actualizacion);
ruta.get('/inventarioActualizar/:pk_id_estante', controladorInventario.Actualizar);
ruta.post('/inventarioIngreso', controladorInventario.Ingresar);
ruta.get('/inventarioIngresoMenu', controladorInventario.Consultar);
ruta.get('/inventarioModificarMenu', controladorInventario.ModificarMenu);
ruta.get('/inventarioEliminar/:pk_id_estante', controladorInventario.Eliminar);

module.exports = ruta;
