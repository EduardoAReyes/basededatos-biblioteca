const express = require ('express');
const ruta = express.Router();

const controladorMenu = require('../Controladores/ControladorMenu');

ruta.get('/menu', controladorMenu.menu);

module.exports = ruta;