const express = require ('express');
const ruta = express.Router();

const controladorConteo_cliente = require('../Controladores/ControladorConteo_cliente');

ruta.get('/clienteConteo', controladorConteo_cliente.Consultar);

module.exports = ruta;