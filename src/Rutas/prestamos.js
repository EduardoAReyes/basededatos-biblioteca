const express = require ('express');
const ruta = express.Router();

const controladorPrestamos = require('../Controladores/ControladorPrestamos');

ruta.post('/prestamosActualizacion/:pk_id_prest', controladorPrestamos.Actualizacion);
ruta.get('/prestamosActualizar/:pk_id_prest', controladorPrestamos.Actualizar);
ruta.post('/prestamosIngreso', controladorPrestamos.Ingresar);
ruta.get('/prestamosIngresoMenu', controladorPrestamos.Consultar);
ruta.get('/prestamosModificarMenu', controladorPrestamos.ModificarMenu);
ruta.get('/prestamosEliminar/:pk_id_prest', controladorPrestamos.Eliminar);

module.exports = ruta;
