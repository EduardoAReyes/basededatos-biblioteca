const express = require ('express');
const ruta = express.Router();

const controladorCliente = require('../Controladores/ControladorCliente');


ruta.post('/Actualizacion/:pk_id_cliente', controladorCliente.Actualizacion);
ruta.get('/clienteActualizar/:pk_id_cliente', controladorCliente.Actualizar);
ruta.post('/clienteIngreso', controladorCliente.Ingresar);
ruta.get('/clienteIngresoMenu', controladorCliente.Consultar);
ruta.get('/clienteModificarMenu', controladorCliente.ModificarMenu);
ruta.get('/clienteEliminar/:pk_id_cliente', controladorCliente.Eliminar);


module.exports = ruta;