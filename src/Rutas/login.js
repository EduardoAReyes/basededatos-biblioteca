const express = require ('express');
const ruta = express.Router();

const controladorLogin = require('../Controladores/ControladorLogin');

ruta.get('/', controladorLogin.login);

module.exports = ruta;