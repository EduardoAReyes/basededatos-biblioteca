const express = require ('express');
const ruta = express.Router();

const controladorLibros = require('../Controladores/ControladorLibros');

ruta.post('/librosActualizacion/:pk_id_lib', controladorLibros.Actualizacion);
ruta.get('/librosActualizar/:pk_id_lib', controladorLibros.Actualizar);
ruta.post('/librosIngreso', controladorLibros.Ingresar);
ruta.get('/librosIngresoMenu', controladorLibros.Consultar);
ruta.get('/librosModificarMenu', controladorLibros.ModificarMenu);
ruta.get('/librosEliminar/:pk_id_lib', controladorLibros.Eliminar);

module.exports = ruta;
