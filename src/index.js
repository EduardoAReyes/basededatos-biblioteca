const express = require('express');
const path = require('path');
const index = express();
const morgan = require('morgan');
const mysql = require('mysql');
const miConexion = require('express-myconnection');


//Declarando rutas
const rutaLogin = require('./Rutas/login');
const rutaMenu = require('./Rutas/menu');
const rutasCliente = require('./Rutas/clientes');
const rutasInventario = require('./Rutas/inventario');
const rutasLibro = require('./Rutas/libros');
const rutasPrestamos = require('./Rutas/prestamos');
const rutaConteo_cliente = require('./Rutas/conteo_cliente');

//Configuracion y creacion del servidor
index.set('port', process.env.PORT || 3000)
index.set('view engine', 'ejs');
index.set('Vistas', path.join(__dirname,'Vistas'));


const host = 'localhost'
const puerto = '3307'
const usuario = 'root'
const contraseña = 'Maria1234000'
const db = 'bibliotecajs'
index.use(morgan('dev'));
index.use(miConexion(mysql, {
    host: host,
    port: puerto,
    user: usuario,
    password: contraseña,
    database: db
}, 'single'))
index.use(express.urlencoded({extended: false}));

//Rutas
index.use('/',rutaLogin);
index.use('/',rutaMenu);
index.use('/',rutasCliente);
index.use('/', rutasInventario);
index.use('/', rutasLibro);
index.use('/', rutasPrestamos);
index.use('/', rutaConteo_cliente);
//Documentos estaticos
index.use(express.static(path.join(__dirname, '/public')));


index.listen(index.get('port'),() => {
    console.log('Servidor en 3000')
});
