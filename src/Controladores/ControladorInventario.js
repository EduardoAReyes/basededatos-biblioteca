const controlador = {};



controlador.Consultar = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM inventario', (err, inventario) => {
            // console.log(clientes)
            res.render('registroInventario', {
                data: inventario
            });
        });
    });
};

controlador.Ingresar = (req, res) => {
    const datos = req.body;
    // console.log(datos)
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO inventario set ?', [datos], (error, inventario) => {
            // console.log(datos)
            // console.log(cliente);
            res.redirect('/inventarioIngresoMenu');
        });
    });
};

controlador.ModificarMenu = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM inventario', (err, inventario) => {
            // console.log(clientes)
            res.render('modificarInventario', {
                data: inventario
            });
        });
    });
};

controlador.Eliminar = (req, res) => {
    // console.log(req.params.pk_id_cliente);
    const {pk_id_estante} = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM inventario WHERE pk_id_estante = ?', [pk_id_estante], (err,inventario) => {
            res.redirect('/inventarioModificarMenu');
        });
    });
};

controlador.Actualizar = (req, res) => {
    const {pk_id_estante} = req.params;
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM inventario WHERE pk_id_estante = ?', [pk_id_estante], (err, inventario) => {
            res.render('modificarActualizacionInventario', {
                data: inventario[0]
            });
        });
    });
};

controlador.Actualizacion = (req, res) => {
    const {pk_id_estante} = req.params;
    const nuevoDato = req.body;
    console.log('Actualizando')
    console.log(nuevoDato)
    req.getConnection((err, conn) => {
        conn.query('UPDATE inventario set ? WHERE pk_id_estante = ?', [nuevoDato, pk_id_estante], (err, inventario) => {
            res.redirect('/inventarioModificarMenu');
        });
    });
};

module.exports = controlador;
