const controlador = {};



controlador.Consultar = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM cliente', (err, clientes) => {
            console.log(clientes)
            res.render('registroCliente', {
                data: clientes
            });
        });
    });
};

controlador.Ingresar = (req, res) => {
    const datos = req.body;
    console.log(datos)
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO cliente set ?', [datos], (error, cliente) => {
            console.log(datos)
            console.log(cliente);
            res.redirect('/clienteIngresoMenu');
        });
    });
};

controlador.ModificarMenu = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM cliente', (err, clientes) => {
            console.log(clientes)
            res.render('modificarCliente', {
                data: clientes
            });
        });
    });
};

controlador.Eliminar = (req, res) => {
    console.log(req.params.pk_id_cliente);
    const {pk_id_cliente} = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM cliente WHERE pk_id_cliente = ?', [pk_id_cliente], (err,clientes) => {
            res.redirect('/clienteModificarMenu');
        });
    });
};

controlador.Actualizar = (req, res) => {
    const {pk_id_cliente} = req.params;
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM cliente WHERE pk_id_cliente = ?', [pk_id_cliente], (err, clientes) => {
            res.render('modificarActualizacion', {
                data: clientes[0]
            });
        });
    });
};

controlador.Actualizacion = (req, res) => {
    const {pk_id_cliente} = req.params;
    const nuevoDato = req.body;
    console.log('Actualizando')
    console.log(nuevoDato)
    req.getConnection((err, conn) => {
        conn.query('UPDATE cliente set ? WHERE pk_id_cliente = ?', [nuevoDato, pk_id_cliente], (err, cliente) => {
            res.redirect('/clienteModificarMenu');
        });
    });
};

module.exports = controlador;
