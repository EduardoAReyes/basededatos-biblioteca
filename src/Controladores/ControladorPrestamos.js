const controlador = {};



controlador.Consultar = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM prestamos', (err, prestamos) => {
            res.render('registroPrestamos', {
                data: prestamos
            });
        });
    });
};

controlador.Ingresar = (req, res) => {
    const datos = req.body;
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO prestamos set ?', [datos], (error, prestamos) => {
            res.redirect('/prestamosIngresoMenu');
        });
    });
};

controlador.ModificarMenu = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM prestamos', (err, prestamos) => {
            res.render('modificarPrestamos', {
                data: prestamos
            });
        });
    });
};

controlador.Eliminar = (req, res) => {
    // console.log(req.params.pk_id_cliente);
    const {pk_id_prest} = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM prestamos WHERE pk_id_prest = ?', [pk_id_prest], (err,prestamos) => {
            res.redirect('/prestamosModificarMenu');
        });
    });
};

controlador.Actualizar = (req, res) => {
    const {pk_id_prest} = req.params;
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM prestamos WHERE pk_id_prest = ?', [pk_id_prest], (err, prestamos) => {
            res.render('modificarActualizacionPrestamos', {
                data: prestamos[0]
            });
        });
    });
};

controlador.Actualizacion = (req, res) => {
    const {pk_id_prest} = req.params;
    const nuevoDato = req.body;
    console.log('Actualizando')
    console.log(nuevoDato)
    req.getConnection((err, conn) => {
        conn.query('UPDATE prestamos set ? WHERE pk_id_prest = ?', [nuevoDato, pk_id_prest], (err, prestamos) => {
            res.redirect('/prestamosModificarMenu');
        });
    });
};

module.exports = controlador;

