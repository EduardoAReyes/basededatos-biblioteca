const controlador = {};



controlador.Consultar = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM libro', (err, libro) => {
            res.render('registroLibros', {
                data: libro
            });
        });
    });
};

controlador.Ingresar = (req, res) => {
    const datos = req.body;
    req.getConnection((err,conn) => {
        conn.query('INSERT INTO libro set ?', [datos], (error, libros) => {
            res.redirect('/librosIngresoMenu');
        });
    });
};

controlador.ModificarMenu = (req, res) => {
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM libro', (err, libros) => {
            res.render('modificarLibros', {
                data: libros
            });
        });
    });
};

controlador.Eliminar = (req, res) => {
    // console.log(req.params.pk_id_cliente);
    const {pk_id_lib} = req.params;
    req.getConnection((err, conn) => {
        conn.query('DELETE FROM libro WHERE pk_id_lib = ?', [pk_id_lib], (err,libros) => {
            res.redirect('/librosModificarMenu');
        });
    });
};

controlador.Actualizar = (req, res) => {
    const {pk_id_lib} = req.params;
    req.getConnection((error, conexion) => {
        conexion.query('SELECT * FROM libro WHERE pk_id_lib = ?', [pk_id_lib], (err, libros) => {
            res.render('modificarActualizacionLibros', {
                data: libros[0]
            });
        });
    });
};

controlador.Actualizacion = (req, res) => {
    const {pk_id_lib} = req.params;
    const nuevoDato = req.body;
    console.log('Actualizando')
    console.log(nuevoDato)
    req.getConnection((err, conn) => {
        conn.query('UPDATE libro set ? WHERE pk_id_lib = ?', [nuevoDato, pk_id_lib], (err, libros) => {
            res.redirect('/librosModificarMenu');
        });
    });
};

module.exports = controlador;

